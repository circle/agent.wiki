## Windows 7

### Telepítés

* Klónozzuk ki a forrásfát a **c:\circle** alá.
* Rakjuk fel a pythont, easy_install-t, pip-et ezek alapján: http://docs.python-guide.org/en/latest/starting/install/win/
* MinGW feltelepítése. http://www.mingw.org/
* Hozzuk létre a C:\python27\Lib\distutils\distutils.cfg fájl tartalmát:

```
[build]
compiler = mingw32
```

* Indítsuk el a PowerShell-t rendszergazdaként. :exclamation: Ha nem így teszünk, akkor fura hibaüzeneteket fogunk kapni!
* Rakjuk be a MinGW-t a PATH-ba: `[Environment]::SetEnvironmentVariable("Path", "$env:Path;C:\MinGW\bin;C:\MinGW\msys\1.0\bin", "User")`
* Letöltjük a pywin32 exe-jét és `easy_install pywin32-*.exe`, figyeljünk, hogy a pythonnal egyező arch legyen.
* Pip-pel feltesszük a szükséges csomagokat, illetve a wmi csomagot: `pip install -r requirements.txt; pip install wmi`
* Ha minden jól alakult, akkor `python agent.py`-vel ki is próbálhatjuk, ekkor az activityk között meg kell, hogy jelenjen az agent indulása.
* Regisztráljuk be Windows szolgáltatásként: `python.exe .\agent-winservice.py --startup auto install`

### exe buildelése
* pip install pyinstaller
* C:\python27\Lib\site-packages\zope\interface\__init__.py másolása C:\python27\Lib\site-packages\zope -ba, ez azért kell, mert különben nem fogja megtalálni a zope.interface-t a pyinstaller
* __init__ az infi mapába ami minden almapát importol
* pyinstaller -F --hidden-import pkg_resources --hidden-import infi agent-winservice.py
* pyinstaller -F -w client.pyw
* ekkor dist-ben ott a két exe

### Telepítés exéből


* Ha van régi context: c:\windows\system32\repl\import\scripts\cloud.bat és c:\context töröl

* c:\circle létrehozás, két fájl bemásolása, client.exe-ről link indítópult-ba
* client.exe-t elindítani kézzel
* powershell rendszergazdaként
```
cd \circle
.\agent_wi.exe --startup auto install
.\agent_wi.exe start
```
* megpróbálni beloginolni a rendes jelszóval, activityben látszik az agnet indulása


## Ubuntu

```bash
apt-get install python-pip python-dev
pip install virtualenvwrapper
. /usr/local/bin/virtualenvwrapper.sh
mkvirtualenv agent
wget -O- https://git.ik.bme.hu/circle/agent/repository/archive.tar.gz |tar xvz
mv agent-master-*/ agent
pip install -r agent/requirements.txt 
```

### 12.04 - 14.04
```bash
cp agent/misc/agent.conf /etc/init/
start agent
```

### 16.04
```bash
cp agent/misc/agent.service /etc/systemd/system
systemctl start agent
```


## Red Hat like distros
Install epel repository.
```bash
yum install python-pip python-devel gcc wget
pip install virtualenvwrapper
. /usr/bin/virtualenvwrapper.sh
mkvirtualenv agent
wget -O- https://git.ik.bme.hu/circle/agent/repository/archive.tar.gz |tar xvz
mv agent.git/ agent
pip install -r agent/requirements.txt
cp agent/misc/agent.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable agent
systemctl start agent
```
